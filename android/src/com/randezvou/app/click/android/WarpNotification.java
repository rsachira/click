package com.randezvou.app.click.android;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;

import com.randezvou.app.click.MyGdxGame;
import com.shephertz.app42.paas.sdk.android.App42API;
import com.shephertz.app42.paas.sdk.android.App42CallBack;
import com.shephertz.app42.paas.sdk.android.storage.Query;
import com.shephertz.app42.paas.sdk.android.storage.QueryBuilder;
import com.shephertz.app42.paas.sdk.android.storage.QueryBuilder.Operator;
import com.shephertz.app42.paas.sdk.android.storage.Storage;
import com.shephertz.app42.paas.sdk.android.storage.Storage.JSONDocument;
import com.shephertz.app42.paas.sdk.android.storage.StorageService;

public class WarpNotification extends Service{
	private static final String DB = "clickDB";
	private static final String gameCollection = "gamedb";
	
	StorageService storage;
	ArrayList<JSONObject> results;
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		storage = initialize(this, "aa0244698e3d3acae2ff137969b2b5c58487a27cfff8d9250eb88466b6b9e5c3", "296c8e165e690550d876e78ad16d2744ae0ef31d90e607b3e7852b0abfa5fbaa");
		results = new ArrayList<JSONObject>();
		
		SharedPreferences prefs = getApplicationContext().getSharedPreferences("click_game_android_prefs", Activity.MODE_PRIVATE);
		String userID = prefs.getString("userID", "0");
		
		if( !userID.equals("0") )
			checkUpdates(userID);
		
		return Service.START_STICKY;
	}
	
	private StorageService initialize(Context c, String apiKey, String secretKey){
		
		App42API.initialize(c, apiKey, secretKey);
		return App42API.buildStorageService();
	}
	
	private void checkUpdates(String userID){
		
		Query q1 = QueryBuilder.build("userA", MyGdxGame.userID, Operator.EQUALS);
		Query q2 = QueryBuilder.build("userB", MyGdxGame.userID, Operator.EQUALS);
		Query q3 = QueryBuilder.build("playing", "update", Operator.EQUALS);
		
		Query query1 = QueryBuilder.compoundOperator(q1, Operator.OR, q2);
		Query query = QueryBuilder.compoundOperator(query1, Operator.AND, q3);
		
		storage.findDocumentsByQuery(DB, gameCollection, query, new App42CallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				Storage s = (Storage)arg0;
				
				for(int i = 0; i < s.getJsonDocList().size(); i++){
					
					JSONDocument jDoc = s.getJsonDocList().get(i);
					JSONTokener t = new JSONTokener( jDoc.getJsonDoc() );
					
					createNotification(jDoc, jDoc.getDocId());
					
					try {
						
						JSONObject j = new JSONObject(t);
						setUpdated(j, jDoc.getDocId() );
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				arg0.printStackTrace();
			}
		});
	}
	
	private void setUpdated(JSONObject j, String docID){
		
		JSONObject json = new JSONObject();
		try{
		
			json.put("userA", j.get("userA") );
			json.put("userA_name",j.get("userA_name") );
			json.put("userB_name", j.get("userB_name") );
			json.put("userB", j.get("userB") );
			json.put("userA_score", j.get("userA_score") );
			json.put("userB_score", j.get("userB_score") );
			json.put("winner", j.get("winner") );
			json.put("playing", "true");
			
			storage.updateDocumentByDocId(DB, gameCollection, docID, json);
		
		}catch(Exception e){
			
			e.printStackTrace();
		}
	}
	
	private void createNotification(JSONDocument j, String id){
		
		NotificationManager mgr = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification note = new Notification(R.drawable.ic_launcher, "Android Example Status message!", System.currentTimeMillis());
		
		Intent launchIntent = new Intent(this, AndroidLauncher.class);
		launchIntent.putExtra("GAME_ID", id);
		launchIntent.putExtra("GAME_DOC", j.getJsonDoc() );
		
		PendingIntent i = PendingIntent.getActivity(this, 0, launchIntent, 0);
		
		note.setLatestEventInfo(this, "Android Example Notification Title", "This is the android example notification message", i);
		mgr.notify(1337, note);
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
