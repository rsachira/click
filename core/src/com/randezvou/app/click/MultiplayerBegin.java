package com.randezvou.app.click;

import org.json.JSONException;
import org.json.JSONObject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.StretchViewport;

public class MultiplayerBegin implements Screen, InputProcessor{
	
	Stage stage;
	MyGdxGame game;
	JSONObject gameObj;
	String ID;
	
	int MODE;
	BitmapFont f;
	
	Label searchingLabel;
	Label playLabel;
	
	Label opName;
	Label urName;
	
	boolean bringDoneUp;
	
	public MultiplayerBegin(MyGdxGame g){
		Gdx.input.setCatchBackKey(true);
		
		stage = new Stage(new StretchViewport(480, 800));
		
		/* Create multiplexer since we need to handle back button */
		InputMultiplexer inMult = new InputMultiplexer(this, stage);
		Gdx.input.setInputProcessor(inMult);
		
		game = g;
		
		MyGdxGame.warp.searchGames();
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		if(MyGdxGame.warp.getLaunch()){
			
			MODE = MyGdxGame.warp.getMode();
			try {
				gameObj = MyGdxGame.warp.getGameObj();
				ID = MyGdxGame.warp.getID();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			searchingLabel.addAction(Actions.sequence(Actions.moveBy(0, -1 * (searchingLabel.getHeight() + 5), 0.4f, Interpolation.linear),
					Actions.run(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							bringDoneUp = true;
						}
					})));
						
			MyGdxGame.warp.setLaunch(false);
		}
		
		if(bringDoneUp){
			
			String opStr;
			if(MODE == MyGdxGame.MULTIPLAYER_BEGIN)
				opStr = "?";
			else{
				
				try {
					opStr = getOpName();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					opStr = "?";
				}
				
			}
			
			opName.setText(opStr);
			playLabel.addAction(Actions.moveBy(0, (playLabel.getHeight() + 10), 2, Interpolation.exp10Out) );
			opName.addAction(Actions.moveBy( -245 + ((240 - opName.getWidth() ) / 2), 0, 2, Interpolation.exp10Out) );
			urName.addAction(Actions.moveBy( 5 + ((240 - urName.getWidth() ) / 2), 0, 2, Interpolation.exp10Out) );
			
			bringDoneUp = false;
		}
		
		stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
		f = new BitmapFont(Gdx.files.internal("balloons_font.fnt"));
		f.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		f.scale(-0.5f);
		f.setColor(Color.WHITE);
		
		LabelStyle searchStyle = new LabelStyle();
		searchStyle.font = f;
		
		Pixmap pMap1 = new Pixmap(100, 100, Format.RGBA8888);
		pMap1.setColor(Color.CLEAR);
		pMap1.fill();
		
		Pixmap pMap2 = new Pixmap(100, 100, Format.RGBA8888);
		pMap2.setColor(Color.YELLOW);
		pMap2.fill();
		
		Skin skin = new Skin();
		skin.add("t", new Texture(pMap1) );
		skin.add("d", new Texture(pMap2) );
		
		searchStyle.background = skin.newDrawable("t");
		
		searchingLabel = new Label("searching...", searchStyle);
		searchingLabel.setPosition(240 - (searchingLabel.getWidth() / 2), 10);
		stage.addActor(searchingLabel);
		
		LabelStyle playStyle = new LabelStyle();
		playStyle.font = f;
		playStyle.background = skin.newDrawable("d");
		
		playLabel = new Label("play", playStyle);
		playLabel.addListener(new InputListener(){
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				
				game.setScreen(new GameScreen(game, MODE, gameObj, ID) );
				return true;
			}
		});
		
		playLabel.setPosition(240 - (playLabel.getWidth() / 2), -1 * (playLabel.getHeight() + 10) );
		stage.addActor(playLabel);
		
		opponentLabel("MachoMan");
		addYourLabel();
	}
	
	private void opponentLabel(String name){
		
		LabelStyle opNameStyle = new LabelStyle();
		opNameStyle.font = MyGdxGame.bitFont;
		
		opName = new Label(name, opNameStyle);
		opName.setPosition( 480 + 5, 800 - opName.getHeight() - 10);
		
		stage.addActor(opName);
	}
	
	private void addYourLabel(){
		
		LabelStyle urNameStyle = new LabelStyle();
		urNameStyle.font = MyGdxGame.bitFont;
		
		urName = new Label("You", urNameStyle);
		urName.setPosition( -5 - urName.getWidth(), 800 - urName.getHeight() - 10);
		
		stage.addActor(urName);
	}
	
	private String getOpName() throws JSONException{
		
		String userA = gameObj.getString("userA");
		String userA_name = gameObj.getString("userA_name");
		String userB_name = gameObj.getString("userB_name");
		
		if( userA.equals(MyGdxGame.userID) )
			return userB_name;
		
		if( userA_name.equals("") )
			return "?";
		
		return userA_name;
	}
	
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		
		if(keycode == Keys.BACK){
			
			game.setScreen(new MultiplayerMenu(game));
			//Dispose stuff;
			return true;
		}
		
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
