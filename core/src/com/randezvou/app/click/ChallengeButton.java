package com.randezvou.app.click;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Group;

public class ChallengeButton extends Group{
	
	ShapeRenderer shape;
	
	public ChallengeButton(){
		
		shape = new ShapeRenderer();
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		batch.end();
		
		shape.setProjectionMatrix( batch.getProjectionMatrix() );
		shape.begin(ShapeType.Filled);
		
		float X = this.getX();
		float Y = this.getY();
		float W = this.getWidth();
		float H = this.getHeight();
		
		shape.setColor(1, 0.65f, 0, 1);
		shape.rect(X, Y, W, H);
		
		shape.setColor(0.82f, 0.41f, 0.12f, 1);
		shape.arc(X + W - 50, Y, 40, 0, 180);
		shape.circle(X + W - 50, Y + 50, 20);
		
		shape.end();
		batch.begin();
	}
}
