package com.randezvou.app.click;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Group;

public class MyGroup extends Group{
	
	ShapeRenderer shape;
	
	public MyGroup(){
		
		shape = new ShapeRenderer();
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		batch.end();
		
		shape.setProjectionMatrix( batch.getProjectionMatrix() );
		shape.begin(ShapeType.Filled);
		
		float X = this.getX();
		float Y = this.getY();
		float W = this.getWidth();
		float H = this.getHeight();
		
		shape.setColor(Color.YELLOW);
		shape.rect(X, Y, W, H);
		
		shape.setColor(Color.GREEN);
		shape.triangle(X + 0.30f * W, Y + 5, X + 0.80f * W, Y + 0.5f * H, X + 0.30f * W, Y + H - 5);
		
		shape.end();
		batch.begin();
	}
}
