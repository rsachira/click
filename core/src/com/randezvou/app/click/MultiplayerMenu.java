package com.randezvou.app.click;

import org.json.JSONException;
import org.json.JSONObject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.randezvou.app.click.shapes.FaceGroup;

public class MultiplayerMenu implements Screen, InputProcessor{
	//public static WarpController warp = null;
	
	private Stage stage;
	Table scrollTable;
	BitmapFont f;
	MyGdxGame g;
	
	Label searchingLabel;
	ScrollPane scrollPane;
	LabelStyle labelStyle;
	boolean bringDoneUp = false;
	
	public MultiplayerMenu(MyGdxGame game){
		
		g = game;
		bringDoneUp = false;
		Gdx.input.setCatchBackKey(true);
		
		if( MyGdxGame.userID.equals("0") ){
			
			try {
				
				MyGdxGame.warp.addUser(MyGdxGame.username, "0");
				
			} catch (JSONException e) {
				// Something went wrong go back to mainmenu
				e.printStackTrace();
				g.setScreen(new MainMenu(g));
			}
			
		}else
			MyGdxGame.warp.getRecentGames();
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		if(MyGdxGame.warp.getLaunch()){
			
			try {
				
				MyGdxGame.warp.changeUsername( MyGdxGame.username );
				MyGdxGame.updateUserID(MyGdxGame.userID);
				MyGdxGame.warp.getRecentGames();
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			MyGdxGame.warp.setLaunch(false);
		}
		
		if( MyGdxGame.warp.getLaunch2() ){
			
			searchingLabel.addAction(Actions.sequence(Actions.moveBy(0, -1 * (searchingLabel.getHeight() + 5), 0.4f, Interpolation.linear),
					Actions.run(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							bringDoneUp = true;
						}
					})));
			
			MyGdxGame.warp.setLaunch2(false);
		}
		
		if(bringDoneUp){
			
			for(int i = 0; i < MyGdxGame.warp.getResults().size(); i++){
				
				JSONObject gameObj = MyGdxGame.warp.getResults().get(i);
				
				String opponent = "";
				boolean won = false;
				
				/* Find Opponent */
				try {
					
					won = gameObj.getString("winner").equals(MyGdxGame.userID);
					
					if( gameObj.getString("userA").equals(MyGdxGame.userID) )
						opponent = gameObj.getString("userB_name");
					else
						opponent = gameObj.getString("userA_name");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if( opponent.equals("") ) opponent = "?";
				
				Label l = new Label(opponent, labelStyle);
				scrollTable.add(l);
				
				FaceGroup fg = new FaceGroup(won);
				fg.setBounds(0, 0, 70, f.getBounds(opponent).height );
				
				scrollTable.add(fg);
				scrollTable.row();
			}
			
			scrollTable.invalidate();
			scrollPane.invalidate();
			scrollPane.addAction(Actions.fadeIn(2) );
			bringDoneUp = false;
		}
		
		stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
		stage = new Stage(new StretchViewport(480, 800));
		
		/* Create an input multiplexer since we need to handle back button */
		InputMultiplexer inMult = new InputMultiplexer(this, stage);
		Gdx.input.setInputProcessor(inMult);
		
		f = new BitmapFont(Gdx.files.internal("balloons_font.fnt"));
		f.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		f.scale(-0.5f);
		f.setColor(Color.WHITE);
		
		labelStyle = new LabelStyle();
		LabelStyle randStyle = new LabelStyle();
		labelStyle.font = f;
		randStyle.font = f;
		
		Pixmap pMap = new Pixmap(100, 100, Format.RGBA8888);
		pMap.setColor(Color.YELLOW);
		pMap.fill();
		
		Skin skin = new Skin();
		skin.add("t", new Texture(pMap) );
		
		randStyle.background = skin.newDrawable("t");
		Label l = new Label("Random", randStyle);
		l.addListener(new InputListener(){
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				
				g.setScreen(new MultiplayerBegin(g) );
				return true;
			}
		});
		
		scrollTable = new Table();
		scrollTable.setFillParent(true);
		scrollTable.top();
		
		scrollTable.add(l);
		scrollTable.row();
		
		scrollPane = new ScrollPane(scrollTable);
		scrollPane.setFillParent(true);
		scrollPane.setColor(1, 1, 1, 0);
		stage.addActor(scrollPane);
		//WarpController.getInstance().startApp("user1");
		
		f = new BitmapFont(Gdx.files.internal("balloons_font.fnt"));
		f.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		f.scale(-0.5f);
		f.setColor(Color.WHITE);
		
		LabelStyle searchStyle = new LabelStyle();
		searchStyle.font = f;
		
		searchingLabel = new Label("searching...", searchStyle);
		searchingLabel.setPosition(240 - (searchingLabel.getWidth() / 2), 10);
		stage.addActor(searchingLabel);
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		
		if(keycode == Keys.BACK){
			
			g.setScreen(new MainMenu(g));
			//Dispose all the stuff;
			return true;
		}
		
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
