package com.randezvou.app.click;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class ProgressBar {
	
	float maxValue;
	float value;
	
	float width;
	float height;
	
	float fill;
	float X, Y;
	
	Color c;
	
	public ProgressBar(float x, float y, float width, float height, float maxValue){
		
		X = x;
		Y = y - height;
		this.width = width;
		this.height = height;
		this.maxValue = maxValue;
		
		value = 0;
	}
	
	public void draw(ShapeRenderer sr){
		
		if( (value/maxValue) > 0.2f )
			sr.setColor(c);
		else
			sr.setColor(0.702f, 0.227f, 0.227f, 1);
		sr.rect(X, Y, fill, height);
		
	}
	
	public void setValue(float val){
		
		value = val;
		fill = width * (value / maxValue);
	}
	
	public void setColor(Color c){
		
		this.c = c;
	}
	
}
