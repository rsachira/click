package com.randezvou.app.click;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class Tile{
	public static Color[] TileColors = new Color[4];
	
	public static float TILE_LENGTH = 70;
	public static float TILE_HEIGHT = 100;
	
	float limXm, limXM, limYm, limYM;	// m = minimum, M = maximum
	
	int numClicks;
	float X;
	float Y;
	Color c;
	
	public Tile(float x, float y, Color c, int clicks){
		
		numClicks = clicks;
		X = x;
		Y = y;
		this.c = c;
		
		generateLimits();
	}
	
	private void generateLimits(){
		
		limXm = X;
		limYm = Y;
		limXM = X + TILE_LENGTH;
		limYM = Y + TILE_HEIGHT;
	}
	
	public boolean clicked(float x, float y){
		
		if( ( (x > limXm) && (x < limXM) ) && ( (y > limYm) && (y < limYM) ) )
			return true;
		
		return false;
	}
	
	public boolean deleteTile(){
		
		numClicks--;
		if(numClicks == 0)
			return true;
		
		return false;
	}
	
	public void draw(ShapeRenderer sr){
		
		sr.setColor(c);
		sr.rect(X, Y, TILE_LENGTH, TILE_HEIGHT);
	}
	
	public void drawText(BitmapFont f, SpriteBatch sb){
		
		String numTxt = Integer.toString(numClicks);
		
		TextBounds tb = f.getBounds(numTxt);
		float dy = (TILE_HEIGHT - 1.5f * tb.height) * 0.5f;
		float dx = (TILE_LENGTH - tb.width) * 0.5f;
		
		f.setColor(1, 1, 1, 1);
		sb.setColor(1, 1, 1, 1);
		f.draw(sb, numTxt, X + dx, (Y + TILE_HEIGHT) - dy );
	}
	
	public boolean isOverlapping(float xm, float xM, float ym, float yM){
		
		boolean c1 = xM >= limXm;
		boolean c2 = xM <= limXM;
		
		boolean c3 = ym >= limYm;
		boolean c4 = ym <= limYM;
		
		boolean c5 = xm >= limXm;
		boolean c6 = xm <= limXM;
		
		boolean c7 = yM <= limYM;
		boolean c8 = yM >= limYm;
		
		return ((c1 && c2) || (c5 && c6)) && ((c3 && c4) || (c7 && c8));
	}
	
	public void move(){
		
		/* Vertically move the tile downwards */
		float step = 3;
		Y -= step;
		
		generateLimits();
	}
	
	public boolean hasDisappeared(){
		
		return (Y <= 0);
	}
}
