package com.randezvou.app.click.android;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.randezvou.app.click.MyGdxGame;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		
		startService(new Intent(this, WarpNotification.class));
		
		String game_id = "";
		String game_doc = "";
		int mode = 0;
		
		if(savedInstanceState != null){
			game_id = savedInstanceState.getString("GAME_ID");
			game_doc = savedInstanceState.getString("GAME_DOC");
		}
		
		if(game_id.equals("") )
			initialize(new MyGdxGame(new AndroidWarp(this)), config);
		else{
			
			JSONTokener t = new JSONTokener(game_doc);
			try {
				
				JSONObject j = new JSONObject(t);
				
				if( j.getString("winner").equals("") )
					mode = 30;
				else
					mode = 20;
				
				initialize(new MyGdxGame(new AndroidWarp(this), j, game_id, mode), config);
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				initialize(new MyGdxGame(new AndroidWarp(this)), config);
			}
		}
		
		
	}
}
