package com.randezvou.app.click.shapes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Group;

public class FaceGroup extends Group{
	
	ShapeRenderer shape;
	boolean hadWon;
	
	public FaceGroup(boolean hadWon){
		
		shape = new ShapeRenderer();
		this.hadWon = hadWon;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		batch.end();
		
		shape.setProjectionMatrix( batch.getProjectionMatrix() );
		shape.begin(ShapeType.Filled);
		
		float X = this.getX();
		float Y = this.getY();
		float W = this.getWidth();
		float H = this.getHeight();
		
		shape.setColor(Color.YELLOW);
		shape.rect(X, Y, W, H);
		
		// Draw eyes
		shape.setColor(Color.GREEN);
		shape.circle(X + W * 0.25f, Y + 0.75f * H, 3);
		shape.circle(X + W * 0.75f, Y + 0.75f * H, 3);
		
		shape.end();
		
		// Draw mouth
		shape.begin(ShapeType.Line);
		
		if( !hadWon )
			shape.arc(X + W * 0.5f, Y + 0.16f * H, W * 0.25f, 0, 180);
		else
			shape.arc(X + W * 0.5f, Y + 0.5f * H, W * 0.25f, 180, 360);
		
		shape.end();
		batch.begin();
	}
}
