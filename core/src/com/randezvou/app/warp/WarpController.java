package com.randezvou.app.warp;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.randezvou.app.click.MyGdxGame;
import com.shephertz.app42.paas.sdk.java.App42API;
import com.shephertz.app42.paas.sdk.java.App42CallBack;
import com.shephertz.app42.paas.sdk.java.storage.Query;
import com.shephertz.app42.paas.sdk.java.storage.QueryBuilder;
import com.shephertz.app42.paas.sdk.java.storage.QueryBuilder.Operator;
import com.shephertz.app42.paas.sdk.java.storage.Storage;
import com.shephertz.app42.paas.sdk.java.storage.Storage.JSONDocument;
import com.shephertz.app42.paas.sdk.java.storage.StorageService;

public class WarpController {
	/*private static final String apiKey = "aa0244698e3d3acae2ff137969b2b5c58487a27cfff8d9250eb88466b6b9e5c3";
	private static final String secretKey = "296c8e165e690550d876e78ad16d2744ae0ef31d90e607b3e7852b0abfa5fbaa";
	private static final String DB = "clickDB";
	private static final String userCollection = "userdb";
	private static final String gameCollection = "gamedb";
	
	private StorageService storageWarp;
	private MyGdxGame g;
	
	public static boolean launch = false;	// Turns this ON when some process is done. Make sure to turn this OFF.
	public int mode = 0;
	public JSONDocument gDoc;
	
	public ArrayList<JSONDocument> results;
	
	public WarpController(MyGdxGame game){
		
		g = game;
		
		WarpController.launch = false;
		mode = 0;
		results = new ArrayList<Storage.JSONDocument>();
		
		init();
		
	}
	
	public void init(){
		
		App42API.initialize(apiKey, secretKey);
		storageWarp = App42API.buildStorageService();
	}
	
	public String addUser(String username) throws JSONException{
		
		JSONObject json = new JSONObject();
		json.put("name",username);
		
		storageWarp.insertJSONDocument(DB, userCollection, json, new App42CallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				
				Storage s = (Storage)arg0;
				MyGdxGame.userID = s.getJsonDocList().get(0).getDocId();
				WarpController.launch = true;
			}
			
			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				
				return;
			}
		});
		
		return MyGdxGame.userID;
	}
	
	public void addNewGame(int score) throws JSONException{
		
		JSONObject json = new JSONObject();
		json.put("userA_score", score);
		json.put("userA", MyGdxGame.userID);
		json.put("userA_name", MyGdxGame.username);
		json.put("userB_score", 0);
		json.put("userB", "");
		json.put("userB_name", "");
		json.put("playing", "false");
		
		storageWarp.insertJSONDocument(DB, gameCollection, json, new App42CallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				WarpController.launch = true;
			}
			
			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				arg0.printStackTrace();
			}
		});
	}
	
	public void searchGames(){
		
		storageWarp.findDocumentByKeyValue(DB, gameCollection, "playing", "false", new App42CallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				
				Storage s = (Storage)arg0;
				gDoc = s.getJsonDocList().get(0);
				mode = MyGdxGame.MULTIPLAYER_REPLY;
				WarpController.launch = true;
				
			}
			
			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				gDoc = null;
				mode = MyGdxGame.MULTIPLAYER_BEGIN;
				WarpController.launch = true;
				return;
			}
		});
	}
	
	public void playGame(int score, JSONDocument game) throws JSONException{
		
		JSONTokener tokener = new JSONTokener(game.getJsonDoc());
		JSONObject gameObj = new JSONObject(tokener);
		
		JSONObject json = new JSONObject();
		json.put("userA_score", gameObj.get("userA_score"));
		
		if(gameObj.getString("userA") == MyGdxGame.userID){
			
			json.put("userA", MyGdxGame.userID);
			json.put("userA_name", MyGdxGame.username);
			json.put("userB_name", gameObj.get("userB_name") );
			json.put("userB", gameObj.get("userB") );
			
		}else{
			
			json.put("userA", gameObj.get("userA"));
			json.put("userA_name", gameObj.get("userA_name") );
			json.put("userB", MyGdxGame.userID);
			json.put("userB_name", MyGdxGame.username);
		}
		
		json.put("userB_score", score);
		json.put("playing", "true");
		
		storageWarp.updateDocumentByDocId(DB, gameCollection, game.getDocId(), json, new App42CallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				WarpController.launch = true;
			}
			
			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				arg0.printStackTrace();
			}
		});
		
	}
	
	public void getReasonGames(){
		
		Query q1 = QueryBuilder.build("userA", MyGdxGame.userID, Operator.EQUALS);
		Query q2 = QueryBuilder.build("userB", MyGdxGame.userID, Operator.EQUALS);
		
		Query query = QueryBuilder.compoundOperator(q1, Operator.OR, q2);
		storageWarp.findDocumentsByQuery(DB, gameCollection, query, new App42CallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				Storage s = (Storage)arg0;
				results = s.jsonDocList;
				WarpController.launch = true;
			}
			
			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				arg0.printStackTrace();
				results = new ArrayList<JSONDocument>();
				WarpController.launch = true;
			}
		});
	}
	
	public void changeUsername(String newUsername) throws JSONException{
		
		JSONObject json = new JSONObject();
		json.put("name", newUsername);
		
		storageWarp.updateDocumentByDocId(DB, userCollection, MyGdxGame.userID, json, new App42CallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				Storage s = (Storage)arg0;
				JSONTokener tokener = new JSONTokener( s.getJsonDocList().get(0).getJsonDoc() );
				try {
					
					JSONObject userObj = new JSONObject(tokener);
					MyGdxGame.updateUsername( userObj.getString("name") );
					WarpController.launch = true;
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					WarpController.launch = true;
				}
			}
			
			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				arg0.printStackTrace();
			}
		});
	}*/
}
