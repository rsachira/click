package com.randezvou.app.warp;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

public interface WarpInterface {
	
	public void init();
	public void addUser(String username, String id) throws JSONException;
	public void addNewGame(int score) throws JSONException;
	public void searchGames();
	public void playGame(int score, JSONObject game, String docID) throws JSONException;
	public void getRecentGames();
	public void changeUsername(String newUsername) throws JSONException;
	
	public boolean getLaunch();
	public void setLaunch(boolean val);
	public boolean getLaunch2();
	public void setLaunch2(boolean val);
	
	public int getMode();
	public JSONObject getGameObj() throws JSONException;
	public ArrayList<JSONObject> getResults();
	public String getID();
}
