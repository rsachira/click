package com.randezvou.app.click;

import java.util.ArrayList;
import java.util.Random;

import org.json.JSONObject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.shephertz.app42.paas.sdk.java.storage.Storage.JSONDocument;

public class GameScreen implements Screen, InputProcessor{
	private static final float GAME_TIME = 60;
	private static final int STAGE_2_STREAK = 7;
	private static final float STAGE_2_TIME = 7;
	private static float elapsedTime = 0;
	private static float absoluteTimeElapsed = 0; //Actual time passed, without time bonuses
	private static boolean gameStarted = false;
	private static float stage_2_start_time = 0;
	
	MyGdxGame game;
	float SCREEN_X_MAX;
	float SCREEN_Y_MAX;
	float SCREEN_X_MIN;
	float SCREEN_Y_MIN;
	int MAX_TILES = 1;
	int SCORE = 0;
	int MULTIPLIER = 1;
	int STREAK = 0;
	
	int MODE;
	JSONObject gameDoc;
	
	OrthographicCamera camera;
	boolean bonusSoundPlayed = false;
	boolean inStageTwo = false;
	
	public static ArrayList<Tile> tiles = new ArrayList<Tile>();
	
	SpriteBatch batch;
	Texture img;
	ShapeRenderer sr;
	BitmapFont scoreFont;
	ProgressBar prog;
	
	String ID;
	
	public GameScreen(MyGdxGame game, int mode, JSONObject gameDoc, String id){
		Gdx.input.setInputProcessor(this);
		
		MODE = mode;
		this.gameDoc = gameDoc;
		
		ID = id;
		
		this.game = game;
		SCREEN_X_MAX = 480 - Tile.TILE_LENGTH - 5;
		SCREEN_Y_MAX = 800 - Tile.TILE_HEIGHT - 5 - 10;
		
		SCREEN_X_MIN = 5;
		SCREEN_Y_MIN = 5;
		
		/* Set Variables to Default values */
		SCORE = 0;
		elapsedTime = 0;
		MAX_TILES = 1;
		MULTIPLIER = 1;
		STREAK = 0;
		
		absoluteTimeElapsed = 0; //Actual time passed, without time bonuses
		gameStarted = false;
		stage_2_start_time = 0;
		bonusSoundPlayed = false;
		inStageTwo = false;
		
		tiles = new ArrayList<Tile>();
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		if(gameStarted){
			elapsedTime += delta;
			absoluteTimeElapsed += delta;
		}
		
		if(elapsedTime >= GAME_TIME){
			
			//End game
			if(MODE == MyGdxGame.SINGLE_PLAYER)
				game.setScreen(new GameOverScreen(game, SCORE));
			else
				game.setScreen(new MultiplayerEnd(MODE, SCORE, gameDoc, ID, game));
			
		}else{
			
			prog.setValue(GAME_TIME - elapsedTime);
		}
		
		batch.setProjectionMatrix(camera.combined);
		sr.setProjectionMatrix(camera.combined);
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		sr.begin(ShapeType.Filled);
			
			for(int i = 0; i < tiles.size(); i++){
				Tile t = tiles.get(i);
				
				/* Move the tiles if the user had entered Stage 2 */
				if(inStageTwo){
					t.move();
					
					if(!bonusSoundPlayed){
						MyGdxGame.bonusLevel.play();
						bonusSoundPlayed = true;
					}
				}
				
				if( t.hasDisappeared() ){
					tiles.remove(i);
					addOnTop();
					
				}else
					t.draw(sr);
			}
			
			prog.draw(sr);
		sr.end();
		
		batch.begin();
			batch.setColor(Color.WHITE);
			
			for(int i = 0; i < tiles.size(); i++){
				
				Tile t = tiles.get(i);
				t.drawText(MyGdxGame.font, batch);
			}
			
			drawScore(batch, scoreFont);
		batch.end();
		
		if( (absoluteTimeElapsed - stage_2_start_time) >= STAGE_2_TIME)
			deactivateStageTwo();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}
	
	private boolean addOnTop(){
		
		float x = MathUtils.random(SCREEN_X_MIN, SCREEN_X_MAX);
		
		Random r = new Random();
		int clicks = r.nextInt(6 - 1) + 1;
		int color = r.nextInt(4 - 1);
		
		Tile t = new Tile(x, (800 - Tile.TILE_HEIGHT), Tile.TileColors[color], clicks);
		
		for(int i = 0; i < tiles.size(); i++){
			
			Tile tile = tiles.get(i);
			if( tile.isOverlapping(t.limXm, t.limXM, t.limYm, t.limYM) )
				return false;
		}
		
		tiles.add(t);
		return true;
	}
	
	private void addTile(){
		
		float x = MathUtils.random(SCREEN_X_MIN, SCREEN_X_MAX);
		float y = MathUtils.random(SCREEN_Y_MIN, SCREEN_Y_MAX);
		
		Random r = new Random();
		int clicks = r.nextInt(6 - 1) + 1;
		int color = r.nextInt(4 - 1);
		
		Tile t = new Tile(x, y, Tile.TileColors[color], clicks);
		
		for(int i = 0; i < tiles.size(); i++){
			
			Tile tile = tiles.get(i);
			if( tile.isOverlapping(t.limXm, t.limXM, t.limYm, t.limYM) )
				return;
		}
		
		tiles.add(t);
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 480, 800);
		
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");
		sr = new ShapeRenderer();
		
		scoreFont = new BitmapFont(Gdx.files.internal("balloons_font.fnt"), false);
		scoreFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		scoreFont.scale(-0.6f);
		scoreFont.setColor(Color.WHITE);
		
		prog = new ProgressBar(0, 800, 480, 10, 60);
		prog.setColor(Color.NAVY);
		prog.setValue(GAME_TIME);
		
		while(tiles.size() < MAX_TILES)
			addTile();
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
		if(MODE == MyGdxGame.SINGLE_PLAYER)
			game.setScreen(new GameOverScreen(game, SCORE));
		else
			game.setScreen(new MultiplayerEnd(MODE, SCORE, gameDoc, ID, game));
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		SCORE = 0;
		tiles.clear();
		elapsedTime = 0;
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		gameStarted = true;
		
		Vector3 touchVec = new Vector3(screenX, screenY, 0);
		camera.unproject(touchVec);
		
		for(int i = 0; i < tiles.size(); i++){
			
			Tile t = tiles.get(i);
			if( t.clicked(touchVec.x, touchVec.y) ){
				
				if( t.deleteTile() ){
					MyGdxGame.tileCompleted.play();
					SCORE += (50 * MULTIPLIER);
					STREAK++;
					
					if(STREAK >= STAGE_2_STREAK && !inStageTwo){
						inStageTwo = true;
						stage_2_start_time = absoluteTimeElapsed;
						STREAK = 3;
						MULTIPLIER = 12;
					}
					
					if(elapsedTime > 0)
						elapsedTime -= 1;
					
					tiles.remove(i);
					
					if(MAX_TILES < 6)
						MAX_TILES++;
					
					while(tiles.size() < MAX_TILES)
						addTile();
					
				}
				MyGdxGame.tileClick.play();
				return true;
			}
		}
		
		/* User had clicked the background */
		MyGdxGame.backgroundClick.play();
		elapsedTime += 10;
		STREAK = 0;
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void drawScore(SpriteBatch sb, BitmapFont font){
		
		String score = Integer.toString(SCORE);
		TextBounds tb = font.getBounds(score);
		font.draw(sb, score, (SCREEN_X_MAX - tb.width + 70), (SCREEN_Y_MAX + 3 + 100) );
	}
	
	private void deactivateStageTwo(){
		
		inStageTwo = false;
		bonusSoundPlayed = false;
		MULTIPLIER = 1;
		for(int i = 0; i < tiles.size(); i++){
			
			Tile t = tiles.get(i);
			if( t.hasDisappeared() )
				tiles.remove(i);
			
		}//End for
		
		while(tiles.size() < MAX_TILES)
			addTile();
	}
}
