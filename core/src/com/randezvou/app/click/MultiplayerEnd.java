package com.randezvou.app.click;

import org.json.JSONException;
import org.json.JSONObject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.randezvou.app.warp.WarpController;

public class MultiplayerEnd implements Screen{
	WarpController warp;
	
	Stage stage;
	Label doneButton;
	Label sendingButton;
	
	int MODE;
	int SCORE;
	JSONObject gameDoc;
	String ID;
	MyGdxGame g;
	
	boolean bringDoneUp = false;
	
	Label opName;
	Label urName;
	Label opScore;
	Label urScore;
	
	public MultiplayerEnd(int mode, int score, JSONObject gameDoc, String id, MyGdxGame game){
		
		MODE = mode;
		this.gameDoc = gameDoc;
		SCORE = score;
		g = game;
		ID = id;
		
		bringDoneUp = false;
		
		stage = new Stage(new StretchViewport(480, 800));
		Gdx.input.setInputProcessor(stage);
		
		if(MODE == MyGdxGame.MULTIPLAYER_BEGIN){
			try {
				MyGdxGame.warp.addNewGame(SCORE);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(MODE == MyGdxGame.MULTIPLAYER_REPLY){
			try {
				MyGdxGame.warp.playGame(SCORE, gameDoc, ID);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		if(MyGdxGame.warp.getLaunch()){
			
			sendingButton.addAction(Actions.sequence(Actions.moveBy(0, -1 * (sendingButton.getHeight() + 5), 0.4f, Interpolation.linear),
					Actions.run(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							bringDoneUp = true;
						}
					})));
			
			MyGdxGame.warp.setLaunch(false);
		}
		
		if(bringDoneUp){
			opName.addAction(Actions.moveBy( -245 + ((240 - opName.getWidth() ) / 2), 0, 2, Interpolation.exp10Out) );
			urName.addAction(Actions.moveBy( 5 + ((240 - urName.getWidth() ) / 2), 0, 2, Interpolation.exp10Out) );
			doneButton.addAction(Actions.moveBy(0, (doneButton.getHeight() + 10), 2, Interpolation.exp10Out) );
			bringDoneUp = false;
		}
		
		stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
	}
	
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
		LabelStyle labelStyle = new LabelStyle();
		labelStyle.font = MyGdxGame.font;
		
		Pixmap pMap1 = new Pixmap(100, 100, Format.RGBA8888);
		pMap1.setColor(Color.CLEAR);
		pMap1.fill();
		
		Pixmap pMap2 = new Pixmap(100, 100, Format.RGBA8888);
		pMap2.setColor(Color.YELLOW);
		pMap2.fill();
		
		Skin skin = new Skin();
		skin.add("t", new Texture(pMap1) );
		skin.add("d", new Texture(pMap2) );
		
		labelStyle.background = skin.newDrawable("t");
		
		sendingButton = new Label("sending...", labelStyle);
		sendingButton.setPosition(240 - (sendingButton.getWidth() / 2), 10);
		stage.addActor(sendingButton);
		
		LabelStyle doneStyle = new LabelStyle();
		doneStyle.font = MyGdxGame.font;
		doneStyle.background = skin.newDrawable("d");
		
		doneButton = new Label("done", doneStyle);
		doneButton.addListener(new InputListener(){
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				
				g.setScreen(new MultiplayerMenu(g));
				return true;
			}
		});
		doneButton.setPosition(240 - (doneButton.getWidth() / 2), -1 * (doneButton.getHeight() + 10) );
		stage.addActor(doneButton);
		
		addYourLabel();
		addYourScoreLabel();
		
		String op = "?";
		String op_score = "-";
		
		try {
			
			if(gameDoc.getString("userA").equals(MyGdxGame.userID) ){
				op = (String)gameDoc.getString("userB_name");
				
				if( gameDoc.getString("winner").equals("") )
					op_score =  gameDoc.getString("userB_score");
				
			}else{
				op =(String)gameDoc.getString("userA_name");
				
				if( gameDoc.getString("winner").equals("") )
					op_score =  gameDoc.getString("userA_score");
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		opponentLabel(op);
		opponentScoreLabel(op_score);
	}
	
	private void opponentLabel(String name){
		
		LabelStyle opNameStyle = new LabelStyle();
		opNameStyle.font = MyGdxGame.bitFont;
		
		opName = new Label(name, opNameStyle);
		opName.setPosition( 480 + 5, 800 - opName.getHeight() - 10);
		
		stage.addActor(opName);
	}
	
	private void addYourLabel(){
		
		LabelStyle urNameStyle = new LabelStyle();
		urNameStyle.font = MyGdxGame.bitFont;
		
		urName = new Label("You", urNameStyle);
		urName.setPosition( -5 - urName.getWidth(), 800 - urName.getHeight() - 10);
		
		stage.addActor(urName);
	}
	
	private void opponentScoreLabel(String score){
		
		LabelStyle opNameStyle = new LabelStyle();
		opNameStyle.font = MyGdxGame.bitFont;
		
		opScore = new Label(score, opNameStyle);
		opScore.setPosition( 480 + 5, 800 - opScore.getHeight() - opName.getHeight() - 10);
		
		stage.addActor(opScore);
	}
	
	private void addYourScoreLabel(){
		
		LabelStyle urNameStyle = new LabelStyle();
		urNameStyle.font = MyGdxGame.bitFont;
		
		urScore = new Label(Integer.toString(SCORE), urNameStyle);
		urScore.setPosition( -5 - urScore.getWidth(), 800 - urScore.getHeight() - urName.getHeight() - 10);
		
		stage.addActor(urScore);
	}
	
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
	
}
