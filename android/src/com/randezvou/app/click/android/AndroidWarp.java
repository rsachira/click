package com.randezvou.app.click.android;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.randezvou.app.click.MyGdxGame;
import com.randezvou.app.warp.WarpInterface;
import com.shephertz.app42.paas.sdk.android.App42API;
import com.shephertz.app42.paas.sdk.android.App42CallBack;
import com.shephertz.app42.paas.sdk.android.storage.Query;
import com.shephertz.app42.paas.sdk.android.storage.QueryBuilder;
import com.shephertz.app42.paas.sdk.android.storage.QueryBuilder.Operator;
import com.shephertz.app42.paas.sdk.android.storage.Storage;
import com.shephertz.app42.paas.sdk.android.storage.Storage.JSONDocument;
import com.shephertz.app42.paas.sdk.android.storage.StorageService;

public class AndroidWarp implements WarpInterface{

	private static final String apiKey = "aa0244698e3d3acae2ff137969b2b5c58487a27cfff8d9250eb88466b6b9e5c3";
	private static final String secretKey = "296c8e165e690550d876e78ad16d2744ae0ef31d90e607b3e7852b0abfa5fbaa";
	private static final String DB = "clickDB";
	private static final String userCollection = "userdb";
	private static final String gameCollection = "gamedb";
	
	private StorageService storageWarp;
	
	public static boolean launch = false;	// Turns this ON when some process is done. Make sure to turn this OFF.
	public static boolean launch2 = false;	// Turns this ON when some process is done. Make sure to turn this OFF.
	
	public int mode = 0;
	public JSONDocument gDoc;
	public Context c;
	
	public ArrayList<JSONObject> results;
	
	public AndroidWarp(Context c){
		
		this.c = c;
		
		AndroidWarp.launch = false;
		mode = 0;
		results = new ArrayList<JSONObject>();
		
		init();
		
	}
	
	public void init(){
		
		App42API.initialize(c, apiKey, secretKey);
		storageWarp = App42API.buildStorageService();
	}
	
	public void addUser(String username, String id) throws JSONException{
		
		if( id.equals("0") ){
			String userJSON = "{\"name\":\"" + username + "\"}"; 
			storageWarp.insertJSONDocument(DB, userCollection, userJSON, new App42CallBack() {
				
				@Override
				public void onSuccess(Object arg0) {
					
					Storage s = (Storage)arg0;
					String user_id = s.getJsonDocList().get(0).getDocId();
					MyGdxGame.updateUserID(user_id);
					
					SharedPreferences myPrefs = c.getSharedPreferences("click_game_android_prefs", Activity.MODE_PRIVATE);
					SharedPreferences.Editor editor = myPrefs.edit();
					editor.putString("userID", user_id);
					editor.commit();
					
					try {
						JSONObject obj = getObject( s.getJsonDocList().get(0).getJsonDoc() );
						MyGdxGame.updateUsername( obj.getString("name") );
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						MyGdxGame.updateUsername("player");
					}
					
					AndroidWarp.launch = true;
				}
				
				@Override
				public void onException(Exception arg0) {
					// TODO Auto-generated method stub
					AndroidWarp.launch = true;
					return;
				}
			});
			
		}else{
			
			changeUsername(username);
		}
		
	}
	
	public void addNewGame(int score) throws JSONException{
		
		JSONObject json = new JSONObject();
		json.put("userA_score", score);
		json.put("userA", MyGdxGame.userID);
		json.put("userA_name", MyGdxGame.username);
		json.put("userB_score", 0);
		json.put("userB", "");
		json.put("userB_name", "");
		json.put("winner", "");
		json.put("playing", "false");
		
		storageWarp.insertJSONDocument(DB, gameCollection, json, new App42CallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				AndroidWarp.launch = true;
			}
			
			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				arg0.printStackTrace();
			}
		});
	}
	
	public void searchGames(){
		
		storageWarp.findDocumentByKeyValue(DB, gameCollection, "playing", "false", new App42CallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				
				Storage s = (Storage)arg0;
				gDoc = s.getJsonDocList().get(0);
				mode = MyGdxGame.MULTIPLAYER_REPLY;
				AndroidWarp.launch = true;
				
			}
			
			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				gDoc = null;
				mode = MyGdxGame.MULTIPLAYER_BEGIN;
				AndroidWarp.launch = true;
				return;
			}
		});
	}
	
	public void playGame(int score, JSONObject gameObj, String docID) throws JSONException{
		
		JSONObject json = new JSONObject();
		
		String winner = "";
		if( gameObj.getString("winner").equals("") ){
			
			// Challenge Excepted.
			if( gameObj.getString("userA").equals(MyGdxGame.userID) ){
				// A Excepted the challenge
				
				String userB = (String)gameObj.get("userB");
				int userB_score = (Integer)gameObj.get("userB_score");
				
				if(score > userB_score) winner = MyGdxGame.userID;
				else winner = userB;
				
				json.put("userA", MyGdxGame.userID);
				json.put("userA_name", MyGdxGame.username);
				json.put("userB_name", gameObj.get("userB_name") );
				json.put("userB", userB );
				json.put("userA_score", score);
				json.put("userB_score", userB_score);
				json.put("winner", winner);
				
			}else{
				
				// B Excepted the challenge
				
				String userA = (String)gameObj.get("userA");
				int userA_score = (Integer)gameObj.get("userA_score");
				
				if(score > userA_score) winner = MyGdxGame.userID;
				else winner = userA;
				
				json.put("userB", MyGdxGame.userID);
				json.put("userB_name", MyGdxGame.username);
				json.put("userA_name", gameObj.get("userA_name") );
				json.put("userA", userA);
				json.put("userA_score", userA_score);
				json.put("userB_score", score);
				json.put("winner", winner);
				
			}
			
		}else{
			
			// Rematch
			if(gameObj.getString("userA").equals(MyGdxGame.userID) ){
				
				// A wants a Rematch
				json.put("userA", MyGdxGame.userID);
				json.put("userA_name", MyGdxGame.username);
				json.put("userB_name", gameObj.get("userB_name") );
				json.put("userB", gameObj.get("userB") );
				json.put("userA_score", score);
				json.put("userB_score", 0);
				json.put("winner", winner);
				
			}else{
				
				// B wants a rematch
				json.put("userB", MyGdxGame.userID);
				json.put("userB_name", MyGdxGame.username);
				json.put("userA_name", gameObj.get("userA_name") );
				json.put("userA", gameObj.get("userA") );
				json.put("userB_score", score);
				json.put("userA_score", 0);
				json.put("winner", winner);
				
			}
		}
		
		json.put("playing", "update");
		
		storageWarp.updateDocumentByDocId(DB, gameCollection, docID, json, new App42CallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				AndroidWarp.launch = true;
			}
			
			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				arg0.printStackTrace();
			}
		});
		
	}
	
	public void getRecentGames(){
		
		Query q1 = QueryBuilder.build("userA", MyGdxGame.userID, Operator.EQUALS);
		Query q2 = QueryBuilder.build("userB", MyGdxGame.userID, Operator.EQUALS);
		
		Query query = QueryBuilder.compoundOperator(q1, Operator.OR, q2);
		storageWarp.findDocumentsByQuery(DB, gameCollection, query, new App42CallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				Storage s = (Storage)arg0;
				
				for(int i = 0; i < s.getJsonDocList().size(); i++){
					
					JSONTokener t = new JSONTokener(s.getJsonDocList().get(i).getJsonDoc() );
					
					try {
						
						JSONObject j = new JSONObject(t);
						results.add(j);
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				AndroidWarp.launch2 = true;
			}
			
			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				arg0.printStackTrace();
				results = new ArrayList<JSONObject>();
				AndroidWarp.launch2 = true;
			}
		});
	}
	
	public void changeUsername(String newUsername) throws JSONException{
		
		JSONObject json = new JSONObject();
		json.put("name", newUsername);
		
		storageWarp.updateDocumentByDocId(DB, userCollection, MyGdxGame.userID, json, new App42CallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				Storage s = (Storage)arg0;
				JSONTokener tokener = new JSONTokener( s.getJsonDocList().get(0).getJsonDoc() );
				try {
					
					JSONObject userObj = new JSONObject(tokener);
					MyGdxGame.updateUsername( userObj.getString("name") );
					AndroidWarp.launch = true;
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					AndroidWarp.launch = true;
				}
			}
			
			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				arg0.printStackTrace();
			}
		});
	}

	@Override
	public boolean getLaunch() {
		
		return launch;
	}

	@Override
	public void setLaunch(boolean val) {
		
		launch = val;
	}
	
	@Override
	public boolean getLaunch2() {
		
		return launch2;
	}

	@Override
	public void setLaunch2(boolean val) {
		
		launch2 = val;
	}
	
	@Override
	public int getMode() {
		// TODO Auto-generated method stub
		return mode;
	}

	@Override
	public JSONObject getGameObj() throws JSONException {
		
		JSONTokener tockener;
		
		if(gDoc == null)
			tockener = new JSONTokener("");
		else
			tockener = new JSONTokener(gDoc.getJsonDoc());
		
		JSONObject gObj = new JSONObject(tockener);
		return gObj;
	}

	@Override
	public ArrayList<JSONObject> getResults() {
		
		return results;
	}
	
	@Override
	public String getID() {
		// TODO Auto-generated method stub
		
		if(gDoc == null)
			return "0";
		
		return gDoc.getDocId();
	}
	
	private JSONObject getObject(String JSONDoc) throws JSONException{
		
		JSONTokener tockener = new JSONTokener(JSONDoc);
		JSONObject obj = new JSONObject(tockener);
		
		return obj;
	}

}
