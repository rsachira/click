package com.randezvou.app.click;

import org.json.JSONException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.utils.viewport.StretchViewport;

public class SettingsScreen implements Screen{
	
	MyGdxGame game;
	BitmapFont f;
	Label updateLabel;
	Label updatingLabel;
	boolean updating = false;
	boolean bringUpdatingLabelUp = false;
	private Stage stage;
	
	TextField nNameField;
	
	public SettingsScreen(MyGdxGame g){
		
		game = g;
		updating = false;
		bringUpdatingLabelUp = false;
		
		stage = new Stage(new StretchViewport(480, 800));
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		if(MyGdxGame.warp.getLaunch() ){
			
			game.setScreen(new MainMenu(game));
			MyGdxGame.warp.setLaunch(false);
		}
		
		if(updating){
			
			updateLabel.addAction(Actions.sequence(Actions.moveBy(0, -1 * (updateLabel.getHeight() + 5), 0.4f, Interpolation.linear),
				Actions.run(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						bringUpdatingLabelUp = true;
					}
				})));
			
			updating = false;
		}
		
		if(bringUpdatingLabelUp){
			
			updatingLabel.addAction(Actions.moveBy(0, (updatingLabel.getHeight() + 10), 2, Interpolation.exp10Out) );
			bringUpdatingLabelUp = false;
			
			try {
				
				if( !nNameField.getText().equals("") )
					MyGdxGame.warp.addUser( nNameField.getText(), MyGdxGame.userID);
				else
					MyGdxGame.warp.addUser( "player", MyGdxGame.userID);
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				MyGdxGame.warp.setLaunch(true);
			}
		}
		
		stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
		f = new BitmapFont(Gdx.files.internal("balloons_font.fnt"));
		f.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		f.scale(-0.5f);
		f.setColor(Color.WHITE);
		
		Pixmap pMap = new Pixmap(100, 100, Format.RGBA8888);
		pMap.setColor(Color.YELLOW);
		pMap.fill();
		
		Skin skin = new Skin();
		skin.add("t", new Texture(pMap) );
		
		float lHeight = nickNameLabel();
		nickNameText(MyGdxGame.username, lHeight);
		
		LabelStyle updateStyle = new LabelStyle();
		updateStyle.font = f;
		updateStyle.background = skin.newDrawable("t");
		
		updateLabel = new Label("update", updateStyle);
		updateLabel.addListener(new InputListener(){
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				
				/* update the system */
				updating = true;
				return true;
			}
			
		});
		
		updateLabel.setPosition(240 - (updateLabel.getWidth() / 2), 0);
		stage.addActor(updateLabel);
		
		LabelStyle updatingStyle = new LabelStyle();
		updatingStyle.font = f;
		
		updatingLabel = new Label("updating..", updatingStyle);
		updatingLabel.setPosition(240 - (updatingLabel.getWidth() / 2), -1 * (updatingLabel.getHeight() + 10) );
		stage.addActor(updatingLabel);
	}
	
	private float nickNameLabel(){
		
		LabelStyle nNameStyle = new LabelStyle();
		nNameStyle.font = MyGdxGame.bitFont;
		
		Label nName = new Label("nick name", nNameStyle);
		nName.setPosition(240 - (nName.getWidth() / 2), 800 - nName.getHeight() );
		stage.addActor(nName);
		
		return nName.getHeight();
	}
	
	private void nickNameText(String username, float labelHeight){
		
		TextFieldStyle nNameTStyle = new TextFieldStyle();
		nNameTStyle.font = f;
		nNameTStyle.fontColor = Color.WHITE;
		
		Pixmap pMap = new Pixmap(2, 100, Format.RGBA8888);
		pMap.setColor(Color.YELLOW);
		pMap.fill();
		
		Skin skin = new Skin();
		skin.add("t", new Texture(pMap) );
		
		nNameField = new TextField(username, nNameTStyle);
		nNameField.setWidth(480 - 10);
		
		nNameTStyle.cursor = skin.newDrawable("t");
		nNameField.setPosition(240 - (nNameField.getWidth() / 2), 800 - labelHeight - nNameField.getHeight());
		stage.addActor(nNameField);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
