package com.randezvou.app.click;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class GameOverScreen implements Screen, InputProcessor{
	MyGdxGame game;
	
	private Stage stage;
	
	int HIGHSCORE;
	Preferences prefs;
	int score;
	boolean highscore = false;
	String titleText = "SCORE";
	
	public GameOverScreen(MyGdxGame g, int score){
		game = g;
		
		prefs = Gdx.app.getPreferences("click_game_prefs");
		HIGHSCORE = prefs.getInteger("highscore", 0);
		
		this.score = score;
		
		if(score > HIGHSCORE){
			
			highscore = true;
			prefs.putInteger("highscore", score);
			prefs.flush();
			
			titleText = "HIGHSCORE";
		}
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
		stage = new Stage(new FitViewport(480, 800));
		
		/* Create Multiplexer */
		InputMultiplexer inMult = new InputMultiplexer(this, stage);
		Gdx.input.setInputProcessor(inMult);
		
		/*Score*/
		LabelStyle style = new LabelStyle();
		style.font = MyGdxGame.font;
		Label scoreLabel = new Label(Integer.toString(score), style);
		scoreLabel.setPosition(240 - (scoreLabel.getWidth() / 2), 400 - ( (scoreLabel.getHeight() / 2) ));
		stage.addActor(scoreLabel);
		
		/* "SCORE" or "NEW HIGHSCORE" label*/
		LabelStyle topicStyle = new LabelStyle();
		topicStyle.font = MyGdxGame.font;
		Label topicLabel = new Label(titleText, topicStyle);
		topicLabel.setPosition(240 - (topicLabel.getWidth() / 2), 400 - (topicLabel.getHeight() / 2) + (scoreLabel.getHeight() / 2) );
		stage.addActor(topicLabel);
		
		MyGroup mg = new MyGroup();
		mg.setPosition(190, 190);
		mg.setWidth(100);
		mg.setHeight(100);
		
		mg.addListener(new InputListener(){
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				
				game.setScreen(new GameScreen(game, MyGdxGame.SINGLE_PLAYER, null, ""));
				
				return true;
			}
		});
		
		stage.addActor(mg);
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		
		if(keycode == Keys.BACK){
			
			game.setScreen(new MainMenu(game));
			return true;
		}
		
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
