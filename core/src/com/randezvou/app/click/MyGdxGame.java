package com.randezvou.app.click;

import org.json.JSONObject;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.randezvou.app.warp.WarpInterface;

public class MyGdxGame extends Game {
	public static final int SINGLE_PLAYER = 10;
	public static final int MULTIPLAYER_BEGIN = 20;
	public static final int MULTIPLAYER_REPLY = 30;
	
	public static BitmapFont font;
	public static BitmapFont bitFont;
	
	/* Game sounds */
	public static Sound tileClick;
	public static Sound backgroundClick;
	public static Sound tileCompleted;
	public static Sound bonusLevel;
	
	public static String username;
	public static String userID;
	
	private static Preferences prefs;
	
	public static WarpInterface warp;
	
	public MyGdxGame(WarpInterface w){
		
		warp = w;
	}
	
	public MyGdxGame(WarpInterface w, JSONObject gameDoc, String id, int mode){
		
		this.setScreen(new GameScreen(this, mode, gameDoc, id));
	}
	
	@Override
	public void create () {
		
		font = new BitmapFont(Gdx.files.internal("balloons_font.fnt"));
		font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		font.scale(-0.3f);
		font.setColor(Color.WHITE);
		
		bitFont = new BitmapFont(Gdx.files.internal("balloons_font.fnt"));
		bitFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		bitFont.scale(-0.7f);
		
		MyGdxGame.prefs = Gdx.app.getPreferences("click_game_prefs");
		username = prefs.getString("username", "player");
		userID = prefs.getString("userid", "0");
		
		tileClick = Gdx.audio.newSound(Gdx.files.internal("sound/tile_click.mp3"));
		backgroundClick = Gdx.audio.newSound(Gdx.files.internal("sound/back_click.mp3"));
		tileCompleted = Gdx.audio.newSound(Gdx.files.internal("sound/smw_coin.wav"));
		bonusLevel = Gdx.audio.newSound(Gdx.files.internal("sound/bonus_level.mp3"));
		
		Tile.TileColors[0] = new Color(1, 0.914f, 0.08f, 1);
		Tile.TileColors[1] = new Color(0.91f, 0.5f, 0.047f, 1);
		Tile.TileColors[2] = new Color(1, 0, 0, 1);
		Tile.TileColors[3] = new Color(0.588f, 0.18f, 0.91f, 1);
		
		this.setScreen(new MainMenu(this));
	}
	
	public static void updateUsername(String username){
		
		MyGdxGame.prefs.putString("username", username);
		MyGdxGame.prefs.flush();
		
		MyGdxGame.username = username;
	}
	
	public static void updateUserID(String id){
		
		MyGdxGame.prefs.putString("userid", id);
		MyGdxGame.prefs.flush();
		
		MyGdxGame.userID = id;
	}
	
}
