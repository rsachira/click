package com.randezvou.app.click;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class MainMenu implements Screen{
	MyGdxGame game;
	
	private Stage stage;
	Preferences prefs;
	BitmapFont highscoreLabelFont;
	int HIGHSCORE = 0;
	
	public MainMenu(MyGdxGame g){
		game = g;
		Gdx.input.setCatchBackKey(false);
		
		highscoreLabelFont = new BitmapFont(Gdx.files.internal("balloons_font.fnt"));
		highscoreLabelFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		highscoreLabelFont.scale(-0.3f);
		highscoreLabelFont.setColor(Color.WHITE);
		
		prefs = Gdx.app.getPreferences("click_game_prefs");
		HIGHSCORE = prefs.getInteger("highscore", 0);
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
		stage = new Stage(new FitViewport(480, 800));
		Gdx.input.setInputProcessor(stage);
		
		/* Title "TAP" */
		LabelStyle style = new LabelStyle();
		style.font = MyGdxGame.font;
		Label titleLabel = new Label("TAP", style);
		titleLabel.setPosition(240 - (titleLabel.getWidth() / 2), 800 - titleLabel.getHeight() - 10 );
		stage.addActor(titleLabel);
		
		/* Mania */
		Label title2Label = new Label("MANIA", style);
		title2Label.setPosition(240 - (title2Label.getWidth() / 2), 800 - titleLabel.getHeight() - (title2Label.getHeight() / 2) + 5 );
		stage.addActor(title2Label);
		
		float clickY = 800 - titleLabel.getHeight() - (title2Label.getHeight() / 2) + 5;
		
		/* "HIGHSCORE" label */
		LabelStyle highscoreStyle = new LabelStyle();
		highscoreLabelFont.scale(-0.3f);
		highscoreStyle.font = highscoreLabelFont;
		Label highscoreLabel = new Label("HIGHSCORE", highscoreStyle);
		highscoreLabel.setPosition(240 - (highscoreLabel.getWidth() / 2), clickY - (highscoreLabel.getHeight() / 2) - 100 );
		stage.addActor(highscoreLabel);
		
		float clickH = clickY - (highscoreLabel.getHeight() / 2) - 100;
		
		/* Highscore label */
		LabelStyle topicStyle = new LabelStyle();
		topicStyle.font = MyGdxGame.font;
		Label highscore = new Label(Integer.toString(HIGHSCORE), topicStyle);
		highscore.setPosition(240 - (highscore.getWidth() / 2), clickH - highscoreLabel.getHeight() );
		stage.addActor(highscore);
		
		MyGroup mg = new MyGroup();
		mg.setPosition(240 - 50, 200);
		mg.setWidth(100);
		mg.setHeight(100);
		
		mg.addListener(new InputListener(){
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				
				game.setScreen(new GameScreen(game, MyGdxGame.SINGLE_PLAYER, null, ""));
				
				return true;
			}
		});
		
		stage.addActor(mg);
		
		/* Challenge button */
		ChallengeButton challengeButton = new ChallengeButton();
		challengeButton.setPosition(240 - 50, 80);
		challengeButton.setWidth(100);
		challengeButton.setHeight(100);
		
		challengeButton.addListener(new InputListener(){
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				
				game.setScreen(new MultiplayerMenu(game) );
				return true;
			}
		});
		
		stage.addActor(challengeButton);
		settingsLabel();
	}
	
	private void settingsLabel(){
		
		Pixmap pMap = new Pixmap(100, 100, Format.RGBA8888);
		pMap.setColor(0.75f, 0.75f, 0.75f, 1);
		pMap.fill();
		
		Skin skin = new Skin();
		skin.add("t", new Texture(pMap) );
		
		BitmapFont settingsFont = new BitmapFont(Gdx.files.internal("balloons_font.fnt"));
		settingsFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		settingsFont.scale(-0.7f);
		settingsFont.setColor(Color.WHITE);
		
		LabelStyle settingsStyle = new LabelStyle();
		settingsStyle.font = settingsFont;
		settingsStyle.background = skin.newDrawable("t");
		
		Label settingsLabel = new Label("settings", settingsStyle);
		settingsLabel.setWidth(480);
		settingsLabel.setPosition(0, 0);
		
		settingsLabel.addListener(new InputListener(){
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				
				game.setScreen(new SettingsScreen(game) );
				return true;
			}
			
		});
		
		stage.addActor(settingsLabel);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
